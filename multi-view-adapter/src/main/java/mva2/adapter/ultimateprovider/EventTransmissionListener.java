/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mva2.adapter.ultimateprovider;

/**
 * 事件传输
 *
 * @since 2021.07.08
 */
public interface EventTransmissionListener {
    /**
     * 统一的事件传输方法
     *
     * @param target 事件发生的场所（一般是某一个布局或者Holder）
     * @param params 事件需要向外部传递的参数
     * @param eventId 当一个view中出现多个事件使用这个参数来区分
     * @param callBack 异步回调返回数据
     * @return 同步返回数据
     */
    Object onEventTransmission(Object target, Object params, int eventId, CallBack callBack);

    /**
     * 回调
     */
    interface CallBack {
        /**
         * 回调
         *
         * @param object 回调传递的内容
         * @return 外部返回的数据，根据自己的需要返回数据
         */
        Object callBack(Object object);
    }
}
