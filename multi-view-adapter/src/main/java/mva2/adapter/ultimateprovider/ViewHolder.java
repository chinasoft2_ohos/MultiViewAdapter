/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package mva2.adapter.ultimateprovider;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.List;

/**
 * ViewHolder
 *
 * @param <M> 对应的模型
 */
public abstract class ViewHolder<M extends Model> implements Notify {
    /**
     * ViewHolder对应的数据
     */
    private M model;
    /**
     * 传递事件到外部（Slice）
     */
    private EventTransmissionListener eventTransmissionListener;
    /**
     * 对应的布局视图
     */
    private Component component;
    /**
     * 当前对应的provider
     */
    private UltimateProvider provider;
    /**
     * 当前在列表中的位置
     */
    private int position;
    /**
     * 当前所在的ListContainer
     */
    private ComponentContainer componentContainer;

    /**
     * 构造方法
     *
     * @param eventTransmissionListener 事件传输对象
     * @param component component
     * @param componentContainer 容器
     * @param provider provider
     */
    public ViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        this.componentContainer = componentContainer;
        this.eventTransmissionListener = eventTransmissionListener;
        this.component = component;
        this.provider = provider;
    }

    /**
     * 当数据已经绑定到ViewMolder。应该在这里给UI控件设置数据
     */
    public abstract void onDataBound();

    /**
     * 返回当前模型数据
     *
     * @return 数据模型
     */
    public M getModel() {
        return model;
    }

    public void setModel(M model) {
        this.model = model;
    }

    public EventTransmissionListener getEventTransmissionListener() {
        return eventTransmissionListener;
    }

    public void setEventTransmissionListener(EventTransmissionListener eventTransmissionListener) {
        this.eventTransmissionListener = eventTransmissionListener;
    }

    public UltimateProvider getProvider() {
        return provider;
    }

    public void setProvider(UltimateProvider provider) {
        this.provider = provider;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    /**
     * 获取上下文
     *
     * @return 上下文对象
     */
    public Context getContext() {
        return component.getContext();
    }

    /**
     * 获取适配器数据数组
     *
     * @return List数组
     */
    public List getList() {
        return getProvider().getModels();
    }

    /**
     * 实例化控件
     *
     * @param id 控件id
     * @return 控件Component
     */
    public Component findComponentById(int id) {
        return getComponent().findComponentById(id);
    }

    public ComponentContainer getComponentContainer() {
        return componentContainer;
    }

    public void setComponentContainer(ComponentContainer componentContainer) {
        this.componentContainer = componentContainer;
    }

    /**
     * 更新当前ItemView
     */
    public void notifyCurrentDataSetItemChanged() {
        notifyDataSetItemChanged(getPosition());
    }

    @Override
    public void notifyDataSetItemChanged(int position) {
        getProvider().notifyDataSetItemChanged(position);
    }
}
