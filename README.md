#  MultiViewAdapter


## 项目介绍
- 项目名称：MultiViewAdapter
- 所属系列：openharmony的第三方组件适配移植
- 功能：所有的布局用一个适配器去实现
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release v2.0.0-beta01

## 效果演示
![screen1](https://images.gitee.com/uploads/images/2021/0526/110137_c7f30c25_1659014.gif "屏幕截图.gif")
![screen1](https://images.gitee.com/uploads/images/2021/0526/105948_ca3a7a1b_1659014.gif "屏幕截图.gif")
![screen1](https://images.gitee.com/uploads/images/2021/0526/110023_82b3bca4_1659014.gif "屏幕截图.gif")
![screen1](https://images.gitee.com/uploads/images/2021/0526/110036_90528b8b_1659014.gif "屏幕截图.gif")



## 安装教程


1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:multi-view-adapter:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
```
   UltimateProvider mProvider = getModels();
   List<Model> mModelList = new UltimateProvider<Model>(mModelList, getContext());
        ...
    mProvider.setModels(mModelList);
        ...
    mListContainer.setItemProvider(mProvider);
```
## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代

- 1.0.0
- 0.0.1-SNAPSHOT

## 版权和许可信息

```
Copyright 2017 Riyaz Ahamed

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
