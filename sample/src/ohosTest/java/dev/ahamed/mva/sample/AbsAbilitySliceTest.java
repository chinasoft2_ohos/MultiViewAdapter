package dev.ahamed.mva.sample;

import dev.ahamed.mva.sample.data.DataManager;
import dev.ahamed.mva.sample.data.model.FaqItem;
import dev.ahamed.mva.sample.data.model.NumberItem;
import dev.ahamed.mva.sample.util.Utils;
import dev.ahamed.mva.sample.view.decoration.DecorationItemModel;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * AbsAbilitySliceTest
 *
 * @since 2021.07.08
 */
public class AbsAbilitySliceTest {
    /**
     * testBundleName
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        Assert.assertEquals("dev.ahamed.mva.sample", actualBundleName);
    }

    /**
     * testGetAccountFaq
     */
    @Test
    public void testGetAccountFaq() {
        DataManager dataManager = new DataManager();
        List<FaqItem> faqItems = dataManager.getAccountFaq();
        Assert.assertEquals(5, faqItems.size());
    }

    /**
     * testGetCast
     */
    @Test
    public void testGetCast() {
        DataManager dataManager = new DataManager();
        List<DecorationItemModel> models = dataManager.getCast();
        Assert.assertEquals(4, models.size());
        if (models.size() > 0) {
            DecorationItemModel model = models.get(0);
            Assert.assertEquals("Tony Stark", model.getName());
            Assert.assertEquals("T", model.getInitials());
        }
    }

    /**
     * testParseDimension
     */
    @Test
    public void testParseDimension() {
        String heightTitleBar = "56vp";
        int dimens = (int) Utils.parseDimension(heightTitleBar);
        Assert.assertEquals(56, dimens);
    }

    /**
     * testGetCrew
     */
    @Test
    public void testGetCrew() {
        DataManager dataManager = new DataManager();
        List<DecorationItemModel> models = dataManager.getCrew();
        Assert.assertEquals(4, models.size());
        if (models.size() > 0) {
            DecorationItemModel model = models.get(0);
            Assert.assertEquals("John Doe", model.getName());
            Assert.assertEquals("J", model.getInitials());
        }
    }

    /**
     * testGetProducers
     */
    @Test
    public void testGetProducers() {
        DataManager dataManager = new DataManager();
        List<DecorationItemModel> models = dataManager.getProducers();
        Assert.assertEquals(4, models.size());
        if (models.size() > 0) {
            DecorationItemModel model = models.get(0);
            Assert.assertEquals("Monty Carlo", model.getName());
            Assert.assertEquals("M", model.getInitials());
        }
    }

    /**
     * testGetMiscFaq
     */
    @Test
    public void testGetMiscFaq() {
        DataManager dataManager = new DataManager();
        List<FaqItem> items = dataManager.getMiscFaq();
        Assert.assertEquals(4, items.size());
        if (items.size() > 0) {
            FaqItem item = items.get(0);
            String answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor "
                    + "incididunt ut labore et dolore magna aliqua.";
            Assert.assertEquals("What is life?", item.getQuestion());
            Assert.assertEquals(answer, item.getAnswer());
        }
    }

    /**
     * testGetNumberItems
     */
    @Test
    public void testGetNumberItems() {
        DataManager dataManager = new DataManager();
        List<NumberItem> items = dataManager.getNumberItems(1000);
        Assert.assertEquals(1000, items.size());
    }

    /**
     * getPaymentFaq
     */
    @Test
    public void getPaymentFaq() {
        DataManager dataManager = new DataManager();
        List<FaqItem> items = dataManager.getPaymentFaq();
        Assert.assertEquals(6, items.size());
        if (items.size() > 1) {
            FaqItem item = items.get(1);
            String answer = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor "
                    + "incididunt ut labore et dolore magna aliqua.";
            Assert.assertEquals("How long will it take for my order to arrive after I make payment?",
                    item.getQuestion());
            Assert.assertEquals(answer, item.getAnswer());
        }
    }
}