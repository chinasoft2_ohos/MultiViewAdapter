package dev.ahamed.mva.sample;

import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @since 2021.07.08
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
