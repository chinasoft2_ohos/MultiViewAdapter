package dev.ahamed.mva.sample.slice;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.view.home.*;
import dev.ahamed.mva.sample.view.widget.DistanceModel;
import mva2.adapter.ultimateprovider.Model;
import mva2.adapter.ultimateprovider.UltimateProvider;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;
import java.util.List;

/**
 * HomeAbilitySlice
 *
 * @since 2021.07.08
 */
public class HomeAbilitySlice extends AbsAbilitySlice {
    private UltimateProvider mProvider;
    private List<Model> mModelList;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void initData() {
        mModelList = getModels();
        mProvider = new UltimateProvider<Model>(mModelList, getContext());
        mListContainer.setItemProvider(mProvider);
        setTvMenuFeatureText(FEATURE_FEATURES);
    }

    @Override
    public void configUpdate() {
    }

    @Override
    public void initConfig() {
    }

    @Override
    public void doItemDismissed(int position) {
    }

    @Override
    public List<Model> getModels() {
        List<Model> list = new ArrayList<Model>();
        list.add(new HomeIntroduceModel());

        list.add(new HomeFeatureModel("Selection Mode", ResourceTable.Graphic_ic_check_box,
                getString(ResourceTable.String_selection_mode)));

        list.add(new HomeFeatureModel("Expansion Mode", ResourceTable.Graphic_ic_expandable_mode,
                getString(ResourceTable.String_expansion_mode)));

        list.add(new HomeFeatureModel("Decoration", ResourceTable.Graphic_ic_decoration,
                getString(ResourceTable.String_decoration)));

        list.add(new HomeFeatureModel("Spans", ResourceTable.Graphic_ic_span_count,
                getString(ResourceTable.String_spans)));

        list.add(new HomeFeatureModel("DiffUtil", ResourceTable.Graphic_ic_diff_util,
                "The library supports the diffutil out-of-the-box. The diffutil can calculate "
                        + "all the changes and apply it to your adapter"));

        list.add(new HomeFeatureModel("Swipe to Dismiss", ResourceTable.Graphic_ic_swipe_to_dismiss,
                "Want to add swipe to dismiss feature? Its as easy as it gets. You can add swipe"
                        + " feature, with listener as well."));

        list.add(new HomeFeatureModel("Drag and Drop", ResourceTable.Graphic_ic_drag,
                "Its easy to add drag and drop support into your adapter. If the view type "
                        + "matches, you can move the item between different sections as well."));

        list.add(new HomeFeatureModel("Infinite Scrolling", ResourceTable.Graphic_ic_infinite_loading,
                "You don't need to create a separate adapter for infinite scrolling feature. "
                        + "Just add the scroll-listener and an helper object."));

        list.add(new HomeFeatureModel("Data Binding", ResourceTable.Graphic_ic_data_binding,
                getString(ResourceTable.String_data_binding)));

        list.add(new HomeFeatureModel("Extensions", ResourceTable.Graphic_ic_extension,
                getString(ResourceTable.String_extensions)));

        list.add(new DistanceModel());
        return list;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
    }
}
