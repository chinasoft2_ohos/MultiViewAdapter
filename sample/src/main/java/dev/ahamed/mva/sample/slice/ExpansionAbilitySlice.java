package dev.ahamed.mva.sample.slice;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.data.model.FaqItem;
import dev.ahamed.mva.sample.view.widget.SwipeLayout;
import mva2.adapter.ultimateprovider.Model;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.*;

/**
 * ExpansionAbilitySlice
 *
 * @since 2021.07.08
 */
public class ExpansionAbilitySlice extends AbsAbilitySlice {
    private Component mHintComponent;
    private SwipeLayout mSwipeLayout;
    private DirectionalLayout mRootLayout;
    private Map<String, List<FaqItem>> mMap;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void initData() {
        initView();
        mMap = new HashMap<>();
        mMap.put("Account", mDataManager.getAccountFaq());
        mMap.put("Payment", mDataManager.getPaymentFaq());
        mMap.put("Life", mDataManager.getMiscFaq());
        addGroupView();

        mRootLayout.addComponent(LayoutScatter.getInstance(getContext()).parse(
                ResourceTable.Layout_item_distance, null, false));

        mScrollView.addComponent(mRootLayout);
        setTvMenuFeatureText(FEATURE_EXPANSION);
    }

    private void initView() {
        mListContainer.setVisibility(Component.HIDE);
        mScrollView.setVisibility(Component.VISIBLE);
        mRootLayout = new DirectionalLayout(getContext());
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_PARENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        mRootLayout.setLayoutConfig(layoutConfig);
        mRootLayout.setOrientation(DirectionalLayout.VERTICAL);

        mRootLayout.addComponent(LayoutScatter.getInstance(getContext()).parse(
                ResourceTable.Layout_item_distance, null, false));
        mHintComponent = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_hint, null, false);
        Text tvHintDescription = (Text) mHintComponent.findComponentById(ResourceTable.Id_tv_hint_description);
        mSwipeLayout = (SwipeLayout) mHintComponent.findComponentById(ResourceTable.Id_swipe_layout);
        mSwipeLayout.setSwipeToDismissListener(this::onItemDismissed, 1);
        tvHintDescription.setText(getContext().getString(ResourceTable.String_expansion_hint));
        mRootLayout.addComponent(mHintComponent);
    }

    private void addGroupView() {
        Set<Map.Entry<String, List<FaqItem>>> entrySet = mMap.entrySet();
        Iterator<Map.Entry<String, List<FaqItem>>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<FaqItem>> entry = iterator.next();
            String header = entry.getKey();
            List<FaqItem> items = entry.getValue();
            Component headerComponent = LayoutScatter.getInstance(getContext()).parse(
                    ResourceTable.Layout_header_expansion, null, false);
            ((Text) headerComponent.findComponentById(ResourceTable.Id_tv_header)).setText(header);
            Button btToggle = (Button) headerComponent.findComponentById(ResourceTable.Id_bt_toggle);
            mRootLayout.addComponent(headerComponent);
            List<Component> groupComponents = new ArrayList<>();
            for (FaqItem item : items) {
                Component itemComponent = LayoutScatter.getInstance(getContext()).parse(
                        ResourceTable.Layout_group_expand, null, false);
                ((Text) itemComponent.findComponentById(ResourceTable.Id_tv_question)).setText(item.getQuestion());
                Image ivToggle = (Image) itemComponent.findComponentById(ResourceTable.Id_iv_toggle);
                Text tvAnswer = (Text) itemComponent.findComponentById(ResourceTable.Id_tv_answer);
                tvAnswer.setText(item.getAnswer());
                ivToggle.setRotation(90);
                ivToggle.setClickedListener(component -> {
                    if (tvAnswer.getVisibility() == Component.VISIBLE) {
                        ivToggle.setRotation(90);
                        tvAnswer.setVisibility(Component.HIDE);
                    } else {
                        ivToggle.setRotation(0);
                        tvAnswer.setVisibility(Component.VISIBLE);
                    }
                });
                groupComponents.add(itemComponent);
            }
            addGroupToRoot(groupComponents);
            btToggle.setClickedListener(component -> {
                if ("COLLAPSE".equals(btToggle.getText())) {
                    btToggle.setText("EXPAND");
                    setGroupVisible(groupComponents, Component.HIDE);
                } else {
                    btToggle.setText("COLLAPSE");
                    setGroupVisible(groupComponents, Component.VISIBLE);
                }
            });
        }
    }

    private void addGroupToRoot(List<Component> groupComponents) {
        for (Component groupComponent : groupComponents) {
            groupComponent.setVisibility(Component.HIDE);
            mRootLayout.addComponent(groupComponent);
        }
    }

    private void setGroupVisible(List<Component> groupComponents, int visible) {
        for (Component groupComponent : groupComponents) {
            groupComponent.setVisibility(visible);
        }
    }

    @Override
    public void configUpdate() {
    }

    @Override
    public void initConfig() {
    }

    @Override
    public List<Model> getModels() {
        return null;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void doItemDismissed(int position) {
        mHintComponent.setVisibility(Component.HIDE);
    }

}
