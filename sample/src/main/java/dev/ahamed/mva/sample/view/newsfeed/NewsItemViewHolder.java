/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.newsfeed;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.util.Utils;
import mva2.adapter.ultimateprovider.EventTransmissionListener;
import mva2.adapter.ultimateprovider.UltimateProvider;
import mva2.adapter.ultimateprovider.ViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.VectorElement;
import ohos.agp.utils.Color;

/**
 * NewsItemViewHolder
 *
 * @since 2021.07.08
 */
public class NewsItemViewHolder extends ViewHolder<NewsItemModel> {
    private Text newsSource;
    private Text title;
    private Image image;
    private Text date;

    public NewsItemViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        super(eventTransmissionListener, component, provider, componentContainer);
        newsSource = (Text) findComponentById(ResourceTable.Id_tv_source);
        title = (Text) findComponentById(ResourceTable.Id_tv_title);
        image = (Image) findComponentById(ResourceTable.Id_image);
        date = (Text) findComponentById(ResourceTable.Id_tv_news_date);
    }

    @Override
    public void onDataBound() {
        final NewsItemModel item = getModel();
        newsSource.setText(item.getSource());
        title.setText(item.getTitle());
        date.setText(item.getTime());
        image.setBackground(Utils.getElementByColorRadius(item.getThumbNailColor(), 20f));
        VectorElement vectorElement = new VectorElement(getContext(), item.getThumbNailId());
        image.setImageElement(vectorElement);

        newsSource.setTextColor(new Color(item.getSourceColor()));
        newsSource.setAroundElements(new VectorElement(getContext(), item.getSourceLogo()), null, null, null);
        if (item.isScroll()) {
            getComponent().setMarginLeft(Utils.vp2px(getContext(), getContext().getString(
                    ResourceTable.String_space_div)));
        }
    }
}
