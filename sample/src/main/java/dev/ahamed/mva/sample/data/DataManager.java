/*
 * Copyright 2017 Riyaz Ahamed
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.data;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.data.model.FaqItem;
import dev.ahamed.mva.sample.data.model.NumberItem;
import dev.ahamed.mva.sample.view.decoration.DecorationItemModel;
import dev.ahamed.mva.sample.view.newsfeed.NewsItemModel;
import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * DataManager
 *
 * @since 2021.07.08
 */
public class DataManager {
    private static final String[] NEWS_COLORS_LIST = new String[] {
            "#ef9a9a",
            "#F48FB1",
            "#CE93D8",
            "#B39DDB",
            "#9FA8DA",
            "#90CAF9",
            "#81D4FA",
            "#C5E1A5",
            "#FFCC80",
            "#FFAB91"
    };
    private static final int[] NEWS_THUMBNAILS = new int[] {
        ResourceTable.Graphic_ic_circle_variant, ResourceTable.Graphic_ic_heart, ResourceTable.Graphic_ic_star
    };
    private static String[] NEWS_DUMMY_TITLES = {
            "Introducing new phone Excellence apps and games on Play ",
            "Life can be tough; here are a few SDK improvements to make it a little easier",
            "You can't rush perfection, but now you can file bugs against it ",
            "Announcing Apps for phone and  developer podcast",
            "Congrats to the new phone Excellence apps and games on  Play "
    };
    private static String[] LANGUAGES = {
            "Arabic", "English", "French", "Hindi", "Russian", "Spanish"
    };
    private static String[] TOPICS = {
            "Local", "World", "Sports", "Economy", "Technology", "Entertainment"
    };
    private static String[] NEWS_PAPERS = {
            "The New York Times", "China Daily", "Times of India", "The Guardian",
            "The Sydney Morning Herald"
    };
    private static String[] TELE_VISIONS = {
            "BBC", "Fox News", "CNN", "NDTV", "Al-Jazeera", "Euronews"
    };
    private static String[] WEBSITES = {
            "Reddit", " News", "Yahoo News", "Huffington Post", "Reuters"
    };
    private static String[] CAST = {
            "Tony Stark", "Hans Olo", "Alice", "Petey Cruiser"
    };
    private static String[] CAST_OVERLINE = {
            "as ROBIN BANKS", "as LUKE WARM", "as MARIA HILL", "as PETE SARIYA"
    };
    private static String[] CREW = {
            "John Doe", "Jane Doe", "Mario Speedwagon", "Rick O'Shea"
    };
    private static String[] CREW_OVERLINE = {
            "DIRECTOR", "DIRECTOR", "WRITER", "WRITER"
    };
    private static String[] PRODUCER = {
            "Monty Carlo", "Jimmy Changa", "Barry Wine", "Buster Hyman"
    };
    private static String[] PRODUCER_OVERLINE = {
            "EXECUTIVE PRODUCER", "PRODUCER", "PRODUCER", "ASSOCIATE PRODUCER"
    };
    private static String[] AUTHORS = {
            "Concurag", "CrazyBilki", "emakerOf", "patA1", "trollicon", "iAmThere", "silento",
            "silentreader"
    };
    private static String[] COMMENTS = {
            "You are so inspiring! Such illustration, many avatar, so sublime",
            "Vastly sick notification :-) Just fab, friend.",
            "Excellent =) I want to make love to the use of button and background image!",
            "Sleek work you have here. Let me take a nap... great shot, anyway.",
            "Nice use of violet in this shot :-) Mission accomplished. It's strong :-)",
            "These are graceful and engaging mate", "I want to learn this kind of shot! Teach me. ",
            "This shot has navigated right into my heart.",
            "Immensely thought out! Excellent shot! Ahhhhhhh...",
            "Nice use of light in this style dude, and Those icons blew my mind.",
            "Button, gradient, boldness, texture – excellent =)",
            "Sky blue. I wonder what would have happened if I made this",
            "This work has navigated right into my heart.",
            "Engaging. It keeps your mind occupied while you wait.",
            "Background, hero, experience, colour – incredible :-)",
            "Very thought out! I wonder what would have happened if I made this",
            "Strong mate I approve the use of layout and lines!", "This is magical work :)",
            "Classic animation! Just delightful!", "Nice use of magenta in this shapes!!"
    };
    private static String[] POSTED_TIME = {
            "Yesterday", "12 hours ago", "5 hours ago", "2 hours ago", "1 hour ago", "5 minutes ago",
            "2 minutes ago", "1 minute ago", "Just now"
    };
    private final SecureRandom mRandom = new SecureRandom();

    /**
     * getAccountFaq
     *
     * @return List<FaqItem>
     */
    public List<FaqItem> getAccountFaq() {
        List<FaqItem> items = new ArrayList<>();
        items.add(new FaqItem("How do I activate my account?"));
        items.add(new FaqItem("How can I change my shipping address?"));
        items.add(new FaqItem("What do you mean by loyalty points? How do I earn it?"));
        items.add(new FaqItem("How can I track my orders & payment?"));
        items.add(new FaqItem("Ho do I change my contact details?"));
        return items;
    }

    /**
     * getCast
     *
     * @return List<DecorationItemModel>
     */
    public List<DecorationItemModel> getCast() {
        List<DecorationItemModel> items = new ArrayList<>(CAST.length);
        int index = 0;
        for (String name : CAST) {
            items.add(new DecorationItemModel(name, name.substring(0, 1).toUpperCase(Locale.ENGLISH),
                    CAST_OVERLINE[index++]));
        }
        return items;
    }

    /**
     * getCrew
     *
     * @return List<DecorationItemModel>
     */
    public List<DecorationItemModel> getCrew() {
        List<DecorationItemModel> items = new ArrayList<>(CREW.length);
        int index = 0;
        for (String name : CREW) {
            items.add(new DecorationItemModel(name, name.substring(0, 1).toUpperCase(Locale.ENGLISH),
                    CREW_OVERLINE[index++]));
        }
        return items;
    }

    /**
     * getProducers
     *
     * @return List<DecorationItemModel>
     */
    public List<DecorationItemModel> getProducers() {
        List<DecorationItemModel> items = new ArrayList<>(PRODUCER.length);
        int index = 0;
        for (String name : PRODUCER) {
            items.add(new DecorationItemModel(name, name.substring(0, 1).toUpperCase(Locale.ENGLISH),
                    PRODUCER_OVERLINE[index++]));
        }
        return items;
    }

    /**
     * getMiscFaq
     *
     * @return List<FaqItem>
     */
    public List<FaqItem> getMiscFaq() {
        List<FaqItem> items = new ArrayList<>();
        items.add(new FaqItem("What is life?"));
        items.add(new FaqItem("What is love?"));
        items.add(new FaqItem("Is the meaning of life the same for animals and humans?"));
        items.add(new FaqItem("Tab? Or Spaces?"));
        return items;
    }

    /**
     * getNewsList
     *
     * @param isOffline 是否为offline消息
     * @param isScroll 能否滚动
     * @return List<NewsItemModel>
     */
    public List<NewsItemModel> getNewsList(boolean isOffline, boolean isScroll) {
        List<NewsItemModel> numberItems = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            int newsSource = mRandom.nextInt(2);
            numberItems.add(new NewsItemModel(i, newsSource == 0 ? "Umbrella News" : "Fake News",
                    newsSource == 0 ? ResourceTable.Graphic_ic_umbrella : ResourceTable.Graphic_ic_fake_news,
                    Color.getIntColor(newsSource == 0 ? "#01CC9D" : "#FE1743"), NEWS_THUMBNAILS[mRandom.nextInt(3)],
                    RgbColor.fromRgbaInt(Color.getIntColor(NEWS_COLORS_LIST[mRandom.nextInt(9)])),
                    NEWS_DUMMY_TITLES[i], "14-July-2018", isOffline, isScroll));
        }
        return numberItems;
    }

    /**
     * getNumberItems
     *
     * @param count 个数
     * @return List<NumberItem>
     */
    public List<NumberItem> getNumberItems(int count) {
        return getNumberItems(1, count);
    }

    /**
     * getNumberItems
     *
     * @param start 开始index
     * @param count 个数
     * @return List<NumberItem>
     */
    public List<NumberItem> getNumberItems(int start, int count) {
        List<NumberItem> numberItems = new ArrayList<>(count);
        for (int i = start; i < count + start; i++) {
            numberItems.add(new NumberItem(i));
        }
        return numberItems;
    }

    /**
     * getPaymentFaq
     *
     * @return List<FaqItem>
     */
    public List<FaqItem> getPaymentFaq() {
        List<FaqItem> items = new ArrayList<>();
        items.add(new FaqItem("Why is there a checkout limit? / What are all the checkout limits?"));
        items.add(new FaqItem("How long will it take for my order to arrive after I make payment?"));
        items.add(new FaqItem("How much is the handling fee?"));
        items.add(new FaqItem("What are the payment methods available?"));
        items.add(new FaqItem("How can I use my remaining Account Credits?"));
        items.add(new FaqItem("What happens if there's been a delivery mishap to my order?"));
        return items;
    }
}
