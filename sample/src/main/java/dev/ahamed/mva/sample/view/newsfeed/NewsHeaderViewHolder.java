/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.newsfeed;

import dev.ahamed.mva.sample.ResourceTable;
import mva2.adapter.ultimateprovider.EventTransmissionListener;
import mva2.adapter.ultimateprovider.UltimateProvider;
import mva2.adapter.ultimateprovider.ViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;

/**
 * NewsHeaderViewHolder
 *
 * @since 2021.07.08
 */
public class NewsHeaderViewHolder extends ViewHolder<NewsHeaderModel> {
    private Text tv_header;

    public NewsHeaderViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        super(eventTransmissionListener, component, provider, componentContainer);
        tv_header = (Text) findComponentById(ResourceTable.Id_tv_header);
    }

    @Override
    public void onDataBound() {
        tv_header.setText(getModel().getHeader());
    }
}
