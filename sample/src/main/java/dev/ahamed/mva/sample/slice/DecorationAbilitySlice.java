package dev.ahamed.mva.sample.slice;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.view.decoration.DecorationHeaderModel;
import dev.ahamed.mva.sample.view.decoration.DecorationItemModel;
import dev.ahamed.mva.sample.view.widget.DistanceModel;
import dev.ahamed.mva.sample.view.widget.HintModel;
import mva2.adapter.ultimateprovider.Model;
import mva2.adapter.ultimateprovider.UltimateProvider;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.VectorElement;

import java.util.ArrayList;
import java.util.List;

/**
 * DecorationAbilitySlice
 *
 * @since 2021.07.08
 */
public class DecorationAbilitySlice extends AbsAbilitySlice {
    private UltimateProvider mProvider;
    private List<Model> mModelList;

    private DependentLayout mSectionDecoration;
    private DependentLayout mHeaderDecoration;
    private DependentLayout mItemDecoration;
    private Image mIvSectionDecoration;
    private Image mIvHeaderDecoration;
    private Image mIvItemDecoration;
    private Button mConfigUpdate;
    private Button mConfigReset;
    private boolean mSectionDecorationChecked = true;
    private boolean mHeaderDecorationChecked = true;
    private boolean mItemDecorationChecked = true;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void initData() {
        mModelList = getModels();
        mProvider = new UltimateProvider<Model>(mModelList, getContext());
        mListContainer.setItemProvider(mProvider);
        setTvMenuFeatureText(FEATURE_DECORATION);
        setTvMenuConfigVisible();

        Component configComponent = LayoutScatter.getInstance(getContext()).parse(
                ResourceTable.Layout_bottom_sheet_decoration_config, null, false);
        mMenuConfig.addComponent(configComponent);
        mSectionDecoration = (DependentLayout) findComponentById(ResourceTable.Id_section_decoration);
        mSectionDecoration.setClickedListener(component -> {
            mSectionDecorationChecked = !mSectionDecorationChecked;
            mIvSectionDecoration.setBackground(new VectorElement(getContext(),
                    mSectionDecorationChecked ? ResourceTable.Graphic_ic_check_box_checked
                            : ResourceTable.Graphic_ic_check_box_unchecked));
        });
        mIvSectionDecoration = (Image) findComponentById(ResourceTable.Id_iv_section_decoration);
        mHeaderDecoration = (DependentLayout) findComponentById(ResourceTable.Id_header_decoration);

        mHeaderDecoration.setClickedListener(component -> {
            mHeaderDecorationChecked = !mHeaderDecorationChecked;
            mIvHeaderDecoration.setBackground(new VectorElement(getContext(),
                    mHeaderDecorationChecked ? ResourceTable.Graphic_ic_check_box_checked :
                            ResourceTable.Graphic_ic_check_box_unchecked));
        });
        mIvHeaderDecoration = (Image) findComponentById(ResourceTable.Id_iv_header_decoration);

        mItemDecoration = (DependentLayout) findComponentById(ResourceTable.Id_item_decoration);
        mItemDecoration.setClickedListener(component -> {
            mItemDecorationChecked = !mItemDecorationChecked;
            mIvItemDecoration.setBackground(new VectorElement(getContext(),
                    mItemDecorationChecked ? ResourceTable.Graphic_ic_check_box_checked :
                            ResourceTable.Graphic_ic_check_box_unchecked));
        });
        mIvItemDecoration = (Image) findComponentById(ResourceTable.Id_iv_item_decoration);
        initConfig();
        mConfigUpdate = (Button) findComponentById(ResourceTable.Id_bt_update);
        mConfigUpdate.setClickedListener(this::onClick);
        mConfigReset = (Button) findComponentById(ResourceTable.Id_bt_reset);
        mConfigReset.setClickedListener(this::onClick);
    }

    @Override
    public void initConfig() {
        mIvSectionDecoration.setBackground(new VectorElement(getContext(), ResourceTable.Graphic_ic_check_box_checked));
        mSectionDecorationChecked = true;
        mIvHeaderDecoration.setBackground(new VectorElement(getContext(), ResourceTable.Graphic_ic_check_box_checked));
        mHeaderDecorationChecked = true;
        mIvItemDecoration.setBackground(new VectorElement(getContext(), ResourceTable.Graphic_ic_check_box_checked));
        mItemDecorationChecked = true;
    }

    @Override
    public void configUpdate() {
        for (Model model : mModelList) {
            if (model instanceof DecorationHeaderModel) {
                ((DecorationHeaderModel) model).setShowHeaderDiv(mHeaderDecorationChecked);
            } else if (model instanceof DecorationItemModel) {
                ((DecorationItemModel) model).setShowShortDiv(mItemDecorationChecked);
                ((DecorationItemModel) model).setShowLongDiv(mSectionDecorationChecked);
            } else {
                continue;
            }
        }
        mProvider.setModels(mModelList);
    }

    @Override
    public List<Model> getModels() {
        List<Model> list = new ArrayList<Model>();
        list.add(new DistanceModel());
        list.add(new HintModel(getContext().getString(ResourceTable.String_decoration_hint), this::onItemDismissed));

        list.add(new DecorationHeaderModel("Cast"));
        list.addAll(mDataManager.getCast());

        list.add(new DecorationHeaderModel("Crew"));
        list.addAll(mDataManager.getCrew());

        list.add(new DecorationHeaderModel("Producers"));
        list.addAll(mDataManager.getProducers());

        list.add(new DistanceModel());
        return list;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void doItemDismissed(int position) {
        mModelList.remove(position);
        mProvider.setModels(mModelList);
    }
}
