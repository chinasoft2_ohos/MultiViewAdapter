package dev.ahamed.mva.sample.slice;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.view.newsfeed.*;
import dev.ahamed.mva.sample.view.widget.DistanceModel;
import dev.ahamed.mva.sample.view.widget.HintModel;
import mva2.adapter.ultimateprovider.Model;
import mva2.adapter.ultimateprovider.UltimateProvider;
import ohos.aafwk.content.Intent;

import java.util.ArrayList;
import java.util.List;

/**
 * NewsFeedAbilitySlice
 *
 * @since 2021.07.08
 */
public class NewsFeedAbilitySlice extends AbsAbilitySlice {
    private UltimateProvider mProvider;
    private List<Model> mModelList;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void initData() {
        mModelList = getModels();
        mProvider = new UltimateProvider<Model>(mModelList, getContext());
        mListContainer.setItemProvider(mProvider);
        setTvMenuFeatureText(FEATURE_NEWSFEED);
    }

    @Override
    public void configUpdate() {
    }

    @Override
    public void initConfig() {
    }

    @Override
    public List<Model> getModels() {
        List<Model> list = new ArrayList<Model>();

        list.add(new DistanceModel());
        list.add(new HintModel(getContext().getString(ResourceTable.String_news_feed_hint), this::onItemDismissed));

        list.addAll(mDataManager.getNewsList(false, false));

        list.add(new NewsHeaderModel("Global News"));
        list.addAll(mDataManager.getNewsList(false, false));

        list.add(new NewsHeaderModel("Trending News"));
        list.addAll(mDataManager.getNewsList(false, false));

        list.add(new NewsScrollModel(mDataManager.getNewsList(false, true)));

        list.addAll(mDataManager.getNewsList(false, false));

        list.add(new NewsHeaderModel("Offline News"));
        list.add(new NewsEmptyModel());
        list.add(new DistanceModel());
        return list;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void doItemDismissed(int position) {
        mModelList.remove(position);
        mProvider.setModels(mModelList);
    }
}
