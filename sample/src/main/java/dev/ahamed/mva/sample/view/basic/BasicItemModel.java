/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.basic;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.data.model.NumberItem;
import mva2.adapter.ultimateprovider.Model;
import ohos.agp.components.Component;

import java.util.List;

/**
 * BasicItemModel
 *
 * @since 2021.07.08
 */
public class BasicItemModel implements Model {
    private List<NumberItem> items;
    private int numColumns = 1;
    private int orientation = Component.VERTICAL;

    public BasicItemModel(List<NumberItem> items, int numColumns) {
        this.items = items;
        this.numColumns = numColumns;
    }

    public List<NumberItem> getItems() {
        return items;
    }

    public int getNumColumns() {
        return numColumns;
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    @Override
    public int getResource(int position) {
        return ResourceTable.Layout_item_basic;
    }

    @Override
    public Class getHolderClass(int position) {
        return BasicItemViewHolder.class;
    }
}
