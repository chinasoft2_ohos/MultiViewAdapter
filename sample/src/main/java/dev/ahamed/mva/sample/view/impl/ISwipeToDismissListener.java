/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.impl;

/**
 * Listener to listen when an item is dismissed by swipe gesture.
 *
 * @param <M> Refers to the model class
 * @since 2021.07.08
 */
public interface ISwipeToDismissListener<M> {
    /**
     * 滑动删除回调.
     *
     * @param position item位置
     * @param item 要删除的对象
     */
    void onItemDismissed(int position, M item);
}
