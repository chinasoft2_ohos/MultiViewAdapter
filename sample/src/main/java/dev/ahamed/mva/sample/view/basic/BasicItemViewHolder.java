/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.basic;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.data.model.NumberItem;
import dev.ahamed.mva.sample.util.Utils;
import mva2.adapter.ultimateprovider.EventTransmissionListener;
import mva2.adapter.ultimateprovider.UltimateProvider;
import mva2.adapter.ultimateprovider.ViewHolder;
import ohos.agp.components.*;

import java.util.List;

/**
 * BasicItemViewHolder
 *
 * @since 2021.07.08
 */
public class BasicItemViewHolder extends ViewHolder<BasicItemModel> {
    private DirectionalLayout basicLayout;
    private int divHeight = 7;

    public BasicItemViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        super(eventTransmissionListener, component, provider, componentContainer);
        basicLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_basic_layout);
    }

    @Override
    public void onDataBound() {
        List<NumberItem> items = getModel().getItems();
        int size = items.size();
        int orientation = getModel().getOrientation();
        ComponentContainer.LayoutConfig basicConfig = basicLayout.getLayoutConfig();
        if (orientation == Component.HORIZONTAL) {
            basicLayout.setOrientation(Component.VERTICAL);
            basicConfig.width = Utils.vp2px(getContext(), getContext().getString(ResourceTable.String_height_titleBar));
            basicConfig.height = Utils.getDisplayHeightInPx(getContext()) - 2 * basicConfig.width;

        } else {
            basicLayout.setOrientation(Component.HORIZONTAL);
            basicConfig.width = DependentLayout.LayoutConfig.MATCH_PARENT;
            basicConfig.height = Utils.vp2px(getContext(),
                    getContext().getString(ResourceTable.String_height_titleBar));
        }

        basicLayout.setLayoutConfig(basicConfig);

        for (int i = 0; i < size; i++) {
            NumberItem item = items.get(i);
            DependentLayout gridComponent = (DependentLayout) LayoutScatter.getInstance(getContext()).parse(
                    ResourceTable.Layout_grid_basic, null, false);
            Text tvText = (Text) gridComponent.findComponentById(ResourceTable.Id_tv_text);
            StackLayout.LayoutConfig gridConfig = new StackLayout.LayoutConfig();
            if (orientation == Component.HORIZONTAL) {
                gridConfig.width = basicConfig.width;
                gridConfig.height = basicLayout.getHeight() / getModel().getNumColumns();
            } else {
                gridConfig.width = Utils.getDisplayWidthInPx(getContext()) / getModel().getNumColumns();
                gridConfig.height = basicConfig.height;
            }
            gridComponent.setLayoutConfig(gridConfig);
            if (orientation == Component.HORIZONTAL) {
                if (getModel().getNumColumns() == 1) {
                    tvText.setHeight(gridConfig.height);
                } else {
                    tvText.setHeight(gridConfig.height - divHeight);
                }
                tvText.setWidth(gridConfig.width - divHeight);
            } else {
                if (getModel().getNumColumns() == 1) {
                    tvText.setWidth(gridConfig.width);
                } else {
                    tvText.setWidth(gridConfig.width - divHeight);
                }
                tvText.setHeight(gridConfig.height - divHeight);
            }
            tvText.setText("" + item.getNumber());
            basicLayout.addComponent(gridComponent);
        }
    }
}
