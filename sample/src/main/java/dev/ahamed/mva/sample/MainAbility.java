package dev.ahamed.mva.sample;

import dev.ahamed.mva.sample.slice.HomeAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * MainAbility
 *
 * @since 2021.07.08
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(HomeAbilitySlice.class.getName());
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().clearFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        getWindow().setStatusBarColor(Color.getIntColor("#CCDFDFDF"));
    }
}
