/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.widget;

import dev.ahamed.mva.sample.ResourceTable;
import mva2.adapter.ultimateprovider.EventTransmissionListener;
import mva2.adapter.ultimateprovider.UltimateProvider;
import mva2.adapter.ultimateprovider.ViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;

/**
 * HintViewHolder
 *
 * @since 2021.07.08
 */
public class HintViewHolder extends ViewHolder<HintModel> {
    private Text tv_hint_description;
    private SwipeLayout swipeLayout;

    public HintViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        super(eventTransmissionListener, component, provider, componentContainer);
        tv_hint_description = (Text) findComponentById(ResourceTable.Id_tv_hint_description);
        swipeLayout = (SwipeLayout) findComponentById(ResourceTable.Id_swipe_layout);
    }

    @Override
    public void onDataBound() {
        tv_hint_description.setText(getModel().getDescription());
        swipeLayout.setSwipeToDismissListener(getModel().getSwipeToDismissListener(), 1);
    }
}
