/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.decoration;

import dev.ahamed.mva.sample.ResourceTable;
import mva2.adapter.ultimateprovider.EventTransmissionListener;
import mva2.adapter.ultimateprovider.UltimateProvider;
import mva2.adapter.ultimateprovider.ViewHolder;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;

/**
 * DecorationItemViewHolder
 *
 * @since 2021.07.08
 */
public class DecorationItemViewHolder extends ViewHolder<DecorationItemModel> {
    private Text tv_avatar;
    private Text tv_name;
    private Text tv_overline;
    private Component shortdiv;
    private Component longdiv;

    public DecorationItemViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        super(eventTransmissionListener, component, provider, componentContainer);
        tv_avatar = (Text) findComponentById(ResourceTable.Id_tv_avatar);
        tv_name = (Text) findComponentById(ResourceTable.Id_tv_name);
        tv_overline = (Text) findComponentById(ResourceTable.Id_tv_overline);
        shortdiv = findComponentById(ResourceTable.Id_shortdiv);
        longdiv = findComponentById(ResourceTable.Id_longdiv);
    }

    @Override
    public void onDataBound() {
        final DecorationItemModel model = getModel();
        tv_avatar.setText(model.getInitials());
        tv_name.setText(model.getName());
        tv_overline.setText(model.getOverline());

        if (getPosition() == getProvider().getCount() - 1 ||
                getProvider().getModels().get(getPosition() + 1) instanceof DecorationHeaderModel) {
            if (shortdiv != null) {
                shortdiv.setVisibility(Component.INVISIBLE);
            }
            if (longdiv != null) {
                longdiv.setVisibility(getModel().isShowLongDiv() ? Component.VISIBLE : Component.HIDE);
            }
        } else {
            if (shortdiv != null) {
                shortdiv.setVisibility(getModel().isShowShortDiv() ? Component.VISIBLE : Component.INVISIBLE);
            }
            if (longdiv != null) {
                longdiv.setVisibility(Component.HIDE);
            }
        }
    }
}
