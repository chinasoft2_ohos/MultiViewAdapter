/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.decoration;

import dev.ahamed.mva.sample.ResourceTable;
import mva2.adapter.ultimateprovider.Model;

/**
 * DecorationItemModel
 *
 * @since 2021.07.08
 */
public class DecorationItemModel implements Model {
    private final String name;
    private final String initials;
    private final String overline;
    private boolean showShortDiv = true;
    private boolean showLongDiv = true;

    public DecorationItemModel(String name, String initials, String overline) {
        this.name = name;
        this.initials = initials;
        this.overline = overline;
    }

    public String getInitials() {
        return initials;
    }

    public String getName() {
        return name;
    }

    public String getOverline() {
        return overline;
    }

    public boolean isShowShortDiv() {
        return showShortDiv;
    }

    public void setShowShortDiv(boolean showShortDiv) {
        this.showShortDiv = showShortDiv;
    }

    public boolean isShowLongDiv() {
        return showLongDiv;
    }

    public void setShowLongDiv(boolean showLongDiv) {
        this.showLongDiv = showLongDiv;
    }

    @Override
    public int getResource(int position) {
        return ResourceTable.Layout_item_person;
    }

    @Override
    public Class getHolderClass(int position) {
        return DecorationItemViewHolder.class;
    }
}
