/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.newsfeed;

import dev.ahamed.mva.sample.ResourceTable;
import mva2.adapter.ultimateprovider.Model;
import ohos.agp.colors.RgbColor;

/**
 * NewsItemModel
 *
 * @since 2021.07.08
 */
public class NewsItemModel implements Model {
    private final int id;
    private final String source;
    private final int sourceLogo;
    private final int sourceColor;

    private final int thumbNailId;
    private final RgbColor thumbNailColor;

    private final String title;
    private final String time;
    private final boolean isOffline;
    private final boolean isScroll;

    public NewsItemModel(int id, String source, int sourceLogo, int sourceColor, int thumbNailId,
            RgbColor thumbNailColor, String title, String time, boolean isOffline, boolean isScroll) {
        this.id = id;
        this.source = source;
        this.sourceLogo = sourceLogo;
        this.sourceColor = sourceColor;
        this.thumbNailId = thumbNailId;
        this.thumbNailColor = thumbNailColor;
        this.title = title;
        this.time = time;
        this.isOffline = isOffline;
        this.isScroll = isScroll;
    }

    public int getId() {
        return id;
    }

    public String getSource() {
        return source;
    }

    public int getSourceColor() {
        return sourceColor;
    }

    public int getSourceLogo() {
        return sourceLogo;
    }

    public RgbColor getThumbNailColor() {
        return thumbNailColor;
    }

    public int getThumbNailId() {
        return thumbNailId;
    }

    public String getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public boolean isOffline() {
        return isOffline;
    }

    public boolean isScroll() {
        return isScroll;
    }

    @Override
    public int getResource(int position) {
        return ResourceTable.Layout_item_news;
    }

    @Override
    public Class getHolderClass(int position) {
        return NewsItemViewHolder.class;
    }
}
