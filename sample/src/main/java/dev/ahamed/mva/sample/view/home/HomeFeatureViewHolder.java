/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.home;

import dev.ahamed.mva.sample.ResourceTable;
import dev.ahamed.mva.sample.util.Utils;
import mva2.adapter.ultimateprovider.EventTransmissionListener;
import mva2.adapter.ultimateprovider.UltimateProvider;
import mva2.adapter.ultimateprovider.ViewHolder;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.render.*;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.io.IOException;
import java.io.InputStream;

/**
 * HomeFeatureViewHolder
 *
 * @since 2021.07.08
 */
public class HomeFeatureViewHolder extends ViewHolder<HomeFeatureModel> {
    private DependentLayout featureHeader;
    private Image iv_feature;
    private Text tv_feature;
    private Text tv_description;

    public HomeFeatureViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        super(eventTransmissionListener, component, provider, componentContainer);
        featureHeader = (DependentLayout) findComponentById(ResourceTable.Id_feature_header);
        iv_feature = (Image) findComponentById(ResourceTable.Id_iv_feature);
        tv_feature = (Text) findComponentById(ResourceTable.Id_tv_feature);
        tv_description = (Text) findComponentById(ResourceTable.Id_tv_description);
    }

    @Override
    public void onDataBound() {
        final HomeFeatureModel feature = getModel();
        PixelMap pixelMap = getPixelMapFromResId(getContext(), ResourceTable.Media_ic_patter);
        featureHeader.setBackground(new PixelMapElement(getRectPixelMap(pixelMap)));

        iv_feature.setImageElement(new VectorElement(getContext(), feature.getIcon()));
        tv_feature.setText(feature.getTitle());
        tv_description.setText(feature.getDescription());
    }

    private PixelMap getPixelMapFromResId(Context context, int resId) {
        InputStream stream = null;
        try {
            stream = context.getResourceManager().getResource(resId);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            ImageSource imageSource = ImageSource.create(stream, sourceOptions);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return PixelMap.create(null);
    }

    private PixelMap getRectPixelMap(PixelMap pixelMap) {
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(Utils.getDisplayWidthInPx(getContext()), featureHeader.getHeight());
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        PixelMap rectPixelMap = PixelMap.create(options);
        Canvas canvas = new Canvas();
        Texture texture = new Texture(rectPixelMap);
        canvas.setTexture(texture);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        PixelMapHolder pixelMapHolder = new PixelMapHolder(PixelMap.create(pixelMap, null));
        PixelMapShader shader = new PixelMapShader(
                pixelMapHolder, Shader.TileMode.REPEAT_TILEMODE, Shader.TileMode.REPEAT_TILEMODE);
        paint.setShader(shader, Paint.ShaderType.PIXELMAP_SHADER);
        canvas.drawRect(0, 0, options.size.width, options.size.height, paint);
        return rectPixelMap;
    }

}
