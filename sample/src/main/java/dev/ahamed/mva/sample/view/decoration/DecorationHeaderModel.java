/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.ahamed.mva.sample.view.decoration;

import dev.ahamed.mva.sample.ResourceTable;
import mva2.adapter.ultimateprovider.Model;

/**
 * DecorationHeaderModel
 *
 * @since 2021.07.08
 */
public class DecorationHeaderModel implements Model {
    private final String header;
    private boolean showHeaderDiv = true;

    public DecorationHeaderModel(String header) {
        this.header = header;
    }

    public String getHeader() {
        return header;
    }

    public boolean isShowHeaderDiv() {
        return showHeaderDiv;
    }

    public void setShowHeaderDiv(boolean showHeaderDiv) {
        this.showHeaderDiv = showHeaderDiv;
    }

    @Override
    public int getResource(int position) {
        return ResourceTable.Layout_header_person;
    }

    @Override
    public Class getHolderClass(int position) {
        return DecorationHeaderViewHolder.class;
    }
}
